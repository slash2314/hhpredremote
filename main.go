package main
/**
 * By Dex Wood
 */
import (
	"bytes"
	"crypto/md5"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

const baseHhpredUrl = "https://toolkit.tuebingen.mpg.de"
const hhpredUrl = baseHhpredUrl + "/api/jobs/?toolName=hhpred"

type HHPredPost struct {
	Alignment      string `json:"alignment"`
	Hhsuitedb      string `json:"hhsuitedb"`
	Proteomes      string `json:"proteomes"`
	MsaGenMethod   string `json:"msa_gen_method"`
	MsaGenMaxIter  string `json:"msa_gen_max_iter"`
	HhpredInclEval string `json:"hhpred_incl_eval"`
	MinSeqidQuery  string `json:"min_seqid_query"`
	MinCov         string `json:"min_cov"`
	SSScoring      string `json:"ss_scoring"`
	AlignMacMode   string `json:"alignmacmode"`
	MacThreshold   string `json:"macthreshold"`
	Desc           string `json:"desc"`
	Pmin           string `json:"pmin"`
	JobID          string `json:"jobID"`
}

func NewHHPredPost(alignment string) HHPredPost {
	hash := md5.New()
	hash.Write([]byte(alignment))
	jobId := fmt.Sprintf("%x", hash.Sum(nil))
	return HHPredPost{
		Alignment:      alignment,
		Hhsuitedb:      "mmcif70/pdb70 NCBI_CD/NCBI_CD scope70/scope70 pfama/pfama",
		Proteomes:      "",
		MsaGenMethod:   "UniRef30",
		MsaGenMaxIter:  "3",
		HhpredInclEval: "1e-3",
		MinSeqidQuery:  "0",
		MinCov:         "20",
		SSScoring:      "2",
		AlignMacMode:   "loc",
		MacThreshold:   "0.3",
		Desc:           "250",
		Pmin:           "20",
		JobID:          jobId,
	}
}

type JobExists struct {
	Exists    bool   `json:"exists"`
	Suggested string `json:"suggested"`
}

func (post HHPredPost) ResultUri() string {
	return baseHhpredUrl + "/api/jobs/" + post.JobID + "/results/files/" + post.JobID + ".hhr"
}
func (post HHPredPost) JobExists() (bool, error) {
	url := baseHhpredUrl + "/api/jobs/check/job-id/" + post.JobID + "/"
	response, err := http.Get(url)
	if err != nil {
		return false, err
	}
	defer response.Body.Close()
	jsonResult, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return false, err
	}
	var jobExists JobExists
	err = json.Unmarshal(jsonResult, &jobExists)
	if err != nil {
		return false, err
	}
	return jobExists.Exists, nil

}

/**
POST json
URL: https://toolkit.tuebingen.mpg.de/api/jobs/?toolName=hhpred
{"alignment":"MFKFVARLNGRTVVTQHETQADATARIGQIAAVNNLTATQERRDEDTISGVWHYRTSPNKGGVAGTFKIIDNSPVRIEDKDGFTADLLNTTDAEVAVVQYDHLPEPTHLDWSHVNRIS",
"hhsuitedb":"mmcif70/pdb70 NCBI_CD/NCBI_CD SMART/SMART pfama/pfama",
"proteomes":"",
"msa_gen_method":"UniRef30",
"msa_gen_max_iter":"3",
"hhpred_incl_eval":"1e-3",
"min_seqid_query":"0",
"min_cov":"20",
"ss_scoring":"2",
"alignmacmode":"loc",
"macthreshold":"0.3",
"desc":"250",
"pmin":"20",
"jobID":"d8e8fca2dc0f896fd7cb4cb0031ba249"
}

*/

func main() {
	seq := flag.String("seq", "", "provide sequence")
	out := flag.String("out", "", "output path")

	flag.Parse()
	if *seq == "" {
		fmt.Println("Sequence cannot be empty")
		flag.Usage()
		return
	}
	if *out == "" {
		fmt.Println("Output path can't be empty")
		flag.Usage()
		return
	}
	//params := createFunctionValues(seq)
	//encoded := params.Encode()
	post := NewHHPredPost(*seq)
	fmt.Println("JobID: " + post.JobID)

	jsonData, err := json.Marshal(post)
	if err != nil {
		fmt.Println("Can't marshall data")
		return
	}
	exists, err := post.JobExists()
	if err != nil {
		fmt.Println("Couldn't retrieve job exists data")
		os.Exit(1)
	}
	dataReader := bytes.NewReader(jsonData)
	if !exists {
		response, err := http.Post(hhpredUrl, "application/json", dataReader)
		if err != nil || response == nil {
			fmt.Println("Couldn't submit request")
			return
		}
		if response.StatusCode != 200 {
			fmt.Println("Couldn't submit job " + response.Status)
			os.Exit(1)
		}
		response.Body.Close()
	} else {
		//If it exists, start it anyway.
		startJob(post)
	}

	fmt.Println("Results will be at " + post.ResultUri())

	start := time.Now()
	for {
		// Check hhpred status
		response, err := http.Get(post.ResultUri())
		if err != nil {
			fmt.Println("Error checking for job data")
			os.Exit(1)
		}
		fmt.Printf("Polling to check if job is finished. Duration %f seconds.\n", time.Since(start).Seconds())
		fmt.Println(response.StatusCode)
		if response.StatusCode == 200 {
			fmt.Println("Results ready")
			all, err := ioutil.ReadAll(response.Body)
			if err != nil {
				fmt.Println("Couldn't get hhpred results")
				os.Exit(1)
			}
			//err = ioutil.WriteFile("/tmp/"+post.JobID+".hhr", all,0664)
			err = ioutil.WriteFile(*out, all, 0664)
			if err != nil {
				fmt.Println("Couldn't write out to result file.")
			}
			fmt.Println("Wrote out to " + *out)
			return
		}
		if time.Since(start).Seconds() > 500 {
			start = time.Now()
			startJob(post)
		}
		response.Body.Close()
		time.Sleep(5 * time.Second)
	}
}

func startJob(post HHPredPost) {
	fmt.Println("Force starting job", post.JobID)
	response, err := http.Get(baseHhpredUrl + "/api/jobs/" + post.JobID + "/start")
	if err != nil {
		fmt.Println("Couldn't start existing job")
		os.Exit(1)
	}
	response.Body.Close()
}
